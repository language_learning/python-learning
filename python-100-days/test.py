import random

for i in "hello world":
    print(i)

print("END")

'''
radius = float(input('请输入圆的半径: '))
perimeter = 2 * 3.1416 * radius
area = 3.1416 * radius * radius
print('周长: %.2f' % perimeter)
print('面积: %.2f' % area)
'''

sum = 0
for x in range(0, 101, 2):
    print(x)
    sum += x
print(sum)

'''
answer = random.randint(1, 100)
counter = 0
while True:
    counter += 1
    num = int(input("Please input your guess number: "))
    if (num > answer):
        print("The answer is lower than your guess number")
    elif (num < answer):
        print("The answer is greater than your guess number")
    else:
        print("Correct!")
        break

if (counter < 6):
    print("You used %d times for testing, great!" % counter)
else:
    print("You used %d times for testing, not good" % counter)
'''

for i in range(1, 10):
    for j in range(1, i+1):
        print("%d*%d=%d" % (i, j, i * j), end = ' ')
        # print("%d" % (i * j), end=' ')
    print("\n")

'''
row = int(input('input row number:'))
for i in range(row):
    for j in range(row):
        if (i + j) >= (row - 1):
            print('*', end='')
        else:
            print(' ', end='')
    print("\n")
'''

for i in range(100, 1000):
    n1 = i // 100
    n2 = i // 10 % 10
    n3 = i % 10
    # print("%f, %f, %f" % (n1, n2, n3))
    if (i == n1**3 + n2**3 + n3**3):
        print("%d, %d, %d" % (n1, n2, n3))

num = int(input("Input a number:"))
rev = 0
if num <= 0:
    print("Input number is equals to or lower than 0")
else:
    while (num > 0):
        rev = rev * 10 + num % 10
        num = num // 10
        print("rev=%d, %d" % (num, rev))
